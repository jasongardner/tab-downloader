#!/usr/bin/env node --harmony
/* jshint esversion: 6 */
'use strict';

const pkg          = require('./package.json');
const fs           = require('fs');
const path         = require('path');
const gulp         = require('gulp');
const connect      = require('gulp-connect');
const sourcemaps   = require('gulp-sourcemaps');
const sass         = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const csso         = require('gulp-csso');
const cmq          = require('gulp-combine-mq');
const uncss        = require('gulp-uncss');
const csscss       = require('gulp-csscss');
const babel        = require('gulp-babel');
const pump         = require('pump');
const uglify       = require('gulp-uglify');
const mustache     = require('gulp-mustache');
const imagemin     = require('gulp-imagemin');
const rename       = require('gulp-rename');
const replace      = require('gulp-replace');
const zip          = require('gulp-zip');
const notify       = require('gulp-notify');
const insert       = require('gulp-insert');
const marked       = require('marked');
const argv         = require('yargs').options({
		'b': {
			alias: ['browser'],
			demand: false,
			describe: 'Define which browser to target',
			nargs: 1,
			string: true,
			default: 'chrome',
			choices: ['chrome', 'edge', 'firefox']
		},
		'p': {
			alias: ['port'],
			demand: false,
			describe: 'Port number used by `http-server` (Default: 8080)',
			nargs: 1,
			number: true,
			default: 8080
		},
		's': {
			alias: ['scope'],
			demand: false,
			describe: 'Targets public or private files in certain tasks',
			nargs: 1,
			string: true,
			default: 'public',
			choices: ['public', 'private']
		}
	}).argv;

marked.setOptions({
	gfm: true,
	tables: true
});

/**
 * Convert a value, of any type, to a string
 * @param val Value to convert
 * @returns {String} A serialized object or `val` cast as a string
 */
function stringify(val) {
	let str;

	if (typeof val === 'object') {
		str = JSON.stringify(val);

		return str.replace(/^\[/, '').replace(/]$/, '');
	}

	return `${val}`;
}

/**
 * The browser name and directory to target in build
 * @type {String}
 */
const BROWSER = (argv.b || 'chrome').trim().toLowerCase();

// Resolve directory paths before assigning constant's values

/**
 * Project directory paths
 * @type {Object}
 */
const DIRS = (() => {
	const exports = {
		root: path.normalize(`./${BROWSER}`),
		node: path.normalize('./node_modules')
	};

	// Path to source files
	exports.src = path.resolve(exports.root, './src');

	// Path to compiled files
	exports.app = path.resolve(exports.root, './app');

	// Path to compiled and compressed files
	exports.dist = path.resolve(exports.root, './dist');

	return exports;
})();

/**
 * File type paths and targets
 * @type {Object}
 */
const FILES = {
	scss: [`${DIRS.src}/**/*.scss`],
	js: {
		public: [`${DIRS.src}/options/**/*.js`],
		private: [`${DIRS.src}/*.js`]
	},
	mustache: [`${DIRS.src}/**/*.mustache`],
	data: path.normalize(`${DIRS.src}/data.json`),
	sprites: `${DIRS.src}/**/symbol-defs.svg`,
	attribution: `${__dirname}/ATTRIBUTION.md`,
	license: `${__dirname}/LICENSE`
};

//
// BUILD TASKS
//

/**
 * Watch targeted files in bulk
 */
gulp.task('watch', () => {
	gulp.watch(FILES.mustache.concat(FILES.data), ['mustache']);
	gulp.watch(FILES.scss, ['scss']);
	gulp.watch(FILES.js.private, ['babel']);
	gulp.watch(FILES.js.public, ['js:options']);
	gulp.watch(FILES.sprites, ['sprites']);
});

/**
 * Start local web server and watch for changes
 */
gulp.task('default', ['connect', 'watch']);

/**
 * Compile ES6/ES2015 JavaScript
 * Babel compiles JavaScript in the browser extension and on the options page.
 */
gulp.task('babel', () => {
	return gulp.src(`${DIRS.src}/*.js`, { base: DIRS.src })
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: [
				'es2015',
				['env', {
					production: {
						presets: 'babili'
					}
				}]
			]
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(DIRS.app))
});

//
// EXTENSION TASKS
//

/**
 * Package extension files
 * (For Firefox)
 */
//gulp.task('zip', () => {
//	return gulp.src(`${DIRS.app}/*`)
//		.pipe(zip((pkg.name || 'extension') + '.xpi'))
//		.pipe(gulp.dest(DIRS.dist));
//});

//
// OPTIONS PAGE TASKS
//

/**
 * Serve options page over localhost during development
 */
gulp.task('connect', () => {
	connect.server({
		name: 'dev',
		root: `${DIRS.app}/options`,
		livereload: true,
		port: 8080
	});
});

gulp.task('js:options', () => {
	return gulp.src(`${DIRS.src}/options/assets/**/*.js`, { base: DIRS.src })
		.pipe(sourcemaps.init())
		.pipe(babel({
			presets: [
				'es2015-script',
				['env', {
					targets: {
						browsers: [`last 2 ${BROWSER} versions`]
					},
					production: {
						presets: 'babili'
					}
				}]
			]
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(DIRS.app));
});

/**
 * Compile Mustache template and trigger LiveReload
 */
gulp.task('mustache', () => {
	const markdown = fs.readFileSync(FILES.attribution, 'utf8'),
		license = fs.readFileSync(FILES.license, 'utf8');

	let lines = license.split('\n');

	const title = lines.shift(),
		body = lines.join('\n').trim();

	return gulp.src(FILES.mustache)
		.pipe(mustache(
			Object.assign(
				{
					basename: function() {
						return function(text, render) {
							const key = text.replace(/[}{]{2}/g, ''); // '{{href}}' -> 'href'

							if (!this.hasOwnProperty(key)) {
								return render(text);
							}

							// 'https://jgardner.s3.amazonaws.com/tab-downloader/demo/1.xml'-> '1.xml'
							return render(this[key].substr(this[key].lastIndexOf('/') + 1));
						}
					}
				},
				pkg,
				JSON.parse(fs.readFileSync(FILES.data)),
				{
					attribution: marked(markdown),
					licenseTitle: title,
					license: marked(body),
					year: (new Date()).getFullYear()
				}
			)
		))
		.pipe(rename({
			extname: '.html'
		}))
		.pipe(gulp.dest(DIRS.app, { base: DIRS.src }))
		.pipe(connect.reload());
});

/**
 * Compile SCSS and trigger LiveReload
 * AutoPrefixer should not attempt to prefix CSS for other browsers
 */
gulp.task('scss', () => {
	return gulp.src(FILES.scss, { base: DIRS.src })
		.pipe(sourcemaps.init())
		.pipe(
			sass({
				outputStyle: 'compressed',
				precision: 6,
				includePaths: [
					DIRS.node
				]
			}).on('error', sass.logError)
		)
		.pipe(autoprefixer({
			browsers: [`last 2 ${BROWSER} versions`],
			cascade: false
		}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(DIRS.app))
		.pipe(connect.reload())
		.pipe(notify('Compiled <%= file.relative %>'));
});

/**
 * Optimize CSS and output to `dist` directory
 * - Removes unused CSS
 * - Combines media queries
 * - Runs CSSO for additional optimization
 */
gulp.task('css', ['scss'], () => {
	return gulp.src([
			`${DIRS.app}/**/*.css`
		])
		.pipe(cmq({
			beautify: false
		}))
		.pipe(uncss({
			html: [`${DIRS.app}/options/index.html`],
			stylesheets: [`${DIRS.app}/options/assets/style.css`],
			htmlroot: `${DIRS.app}/options`
		}))
		.pipe(csso({
			restructure: true
		}))
		.pipe(gulp.dest(DIRS.dist));
});

/**
 * Lint CSS
 * - Checks for duplicate CSS properties
 */
gulp.task('css:lint', ['scss'], () => {
	return gulp.src(`${DIRS.app}/options/assets/style.css`)
		.pipe(csscss());
});

//
// IMAGES
//

/**
 * Optimize IcoMoon sprites then rename and copy to options page assets
 */
gulp.task('sprites', () => {
	return gulp.src(FILES.sprites, { base: DIRS.src })
		//.pipe(imagemin())
		.pipe(rename({
			basename: 'sprites'
		}))
		.pipe(gulp.dest(DIRS.app));
});
