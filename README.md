Tab Downloader
==============

A browser extension to download open tabs in bulk.

[![Download from Chrome Web Store](https://jgardner.s3.amazonaws.com/tab-downloader/readme-webstore-download.png)](https://chrome.google.com/webstore/)


## Changelog ##

...

### Credits ###
_Tab Downloader_ created by [Jason Gardner](http://jasongardner.co). Please see [ATTRIBUTION.md](ATTRIBUTION.md) for a list of kind, generous, and talented people whose work is used in this project. 
